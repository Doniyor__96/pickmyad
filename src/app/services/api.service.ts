import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  url: string = 'https://demo8991588.mockable.io/';

  constructor(
    private http: HttpClient
  ) {
  }

  getBookings(): Observable<any> {
    return this.http.get(this.url + 'card');
  }

}
