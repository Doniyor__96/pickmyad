import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Column} from "../models/column.model";
import {Board} from "../models/board.model";
import {ApiService} from "../services/api.service";
import {Booking} from "../models/booking.model";

@Component({
  selector: 'app-booking-board',
  templateUrl: './booking-board.component.html',
  styleUrls: ['./booking-board.component.scss']
})
export class BookingBoardComponent implements OnInit {
  board: Board = new Board([
    new Column(1, 'Booking pending', []),
    new Column(2, 'Booking accepted', []),
    new Column(3, 'Production inprogress', []),
    new Column(4, 'Job Completed', []),
    new Column(5, 'Booking Rejected', [])
  ]);

  loading: boolean = true;

  constructor(
    private apiService: ApiService
  ) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.apiService.getBookings().subscribe({
        next: (result: any) => {
          for (const nextKey in result) {
            for (const boardKey in this.board.columns) {
              if (this.board.columns[boardKey].id === result[nextKey].columnId) {
                this.board.columns[boardKey].bookings.push(result[nextKey])
              }
            }
          }

          this.loading = false;
          console.log(this.board)
        },
        error: error => {
          console.log(error)
        }
      }
    )
  }

  drop(event: CdkDragDrop<Booking[], any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
    }
  }

}
