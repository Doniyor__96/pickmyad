import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BookingBoardComponent} from "./booking-board/booking-board.component";

const routes: Routes = [
  {path: '', redirectTo: '/booking-board', pathMatch: 'full'},
  {path: 'booking-board', component: BookingBoardComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
