import {Booking} from "./booking.model";

export class Column {
  constructor(public id: number, public title: string, public bookings: Booking[]) {

  }
}

