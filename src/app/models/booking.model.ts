export class Booking {
  constructor(
    public id: string,
    public status: string,
    public price: number,
    public avatar: string,
    public name: string,
    public title: string,
    public bookingDate: string,
    public livingDate: string,
    public icons: []
  ) {
  }
}
